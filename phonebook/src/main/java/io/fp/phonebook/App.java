package io.fp.phonebook;

public class App {
    public static void main(String... args){
        PhoneBook b1 = new PhoneBook();

       try {
            b1.createContact("Michi Be", 111, 888, 999);
            b1.createContact("Martin Pat", 222);
            b1.createContact("Michi Be", 333);
            b1.createContact("Martin Pat", 444);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Ausgabe Person mit Nummer 222 und restliche Nummern.");
        System.out.println("Name: " + b1.getPerson(222) + " Numbers: " + b1.getPhoneNumbers(b1.getPerson(222)));
        System.out.println();
        System.out.println("Ausgabe gesamtes Buch.");
        System.out.println(b1.getBook());
        System.out.println();

        System.out.println("Ausgabe Nummern l�schen von Michi Be.");
        System.out.println(b1.deleteEntriesOfAPerson("Michi Be"));
        System.out.println();
        System.out.println("Ausgabe Nummer 222 l�schen.");
        System.out.println(b1.deleteEntryByPhoneNumber(222));
        System.out.println();
        System.out.println("Ausgabe gesamtes Buch.");
        System.out.println(b1.getBook());
    }
}
