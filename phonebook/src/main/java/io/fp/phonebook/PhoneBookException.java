package io.fp.phonebook;
/**
 * Thrown to indicate that something in a method of a {@link PhoneBook} has been failed.
 */
public class PhoneBookException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8861060650714370603L;

	/**
     * Constructs an <code>PhoneBookException</code> with the
     * specified detail message.
     *
     * @param message the detail message.
     */
    public PhoneBookException(String message) {
        super(message);
    }
}