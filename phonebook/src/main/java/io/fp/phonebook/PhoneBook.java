package io.fp.phonebook;
import java.util.*;

/**
 * An object that manages contacts. For this purpose it is using the TreeMap implementation.
 * A phone book cannot contain duplicate phone numbers (<strong>K</strong>eys),
 * but it can contain duplicate persons (<strong>V</strong>alues).
 */
public class PhoneBook {
    private SortedMap<Integer, String> book;
    
    /**
     * Initializes the variable {@link book} in the constructor.
     */
    public PhoneBook() {
    	book = new TreeMap<Integer, String>();
    }

    /**
     * Creates a new contact entry in the phone book.
     * Generates for every key element <tt>phoneNumbers</tt> a map entry.
     * If even a single phone number is already in use in the phone book this method will throw a {@link PhoneBookException}.
     * No numbers should be added at all, if a {@link PhoneBookException} is thrown.
     *
     * @param person name of the contact person
     * @param phoneNumbers all phone numbers of the contact person
     * @throws PhoneBookException if an entered phone number is already in use. The exception message contains the related number and the person's name which is using this number.
     */
    public void createContact(String person, Integer... phoneNumbers) throws PhoneBookException{
        for(Integer num : phoneNumbers){
            if(book.containsKey(num)){
                throw new PhoneBookException("Number '"+num+ "' is already used by "+book.get(num));
            }
        }
        for(Integer num : phoneNumbers){
                book.put(num, person);
        }
    }

     /**
     * Returns the person's name to which the specified phone number is mapped,
     * or {@code null} if this phone book contains no mapping for the phone number.
     * @param phoneNumber of the person
     * @return  person's name
     */
    public String getPerson(Integer phoneNumber){
        return book.get(phoneNumber);
       }

    /**
     * Returns a set of all phone numbers of a specified person.
     * @param person specified person
     * @return all phone numbers of the person
     */
    public Set<Integer> getPhoneNumbers(String person){
        Set<Integer> foundPhoneNumbers = new HashSet<Integer>();

        for(Map.Entry<Integer,String> entry : book.entrySet()){
            if(entry.getValue().equals(person)){
                foundPhoneNumbers.add((Integer)entry.getKey());
            }
        }
        return foundPhoneNumbers;
    }

    /**
     * Deletes the phone book entry which matches the specified phone number and returns the persons's name of the deleted entry,
     * or {@code null} if this phone book contains no mapping for the phone number.
     * @param phoneNumber of the entry which will be deleted
     * @return the person's name of the deleted entry
     */
    public String deleteEntryByPhoneNumber(Integer phoneNumber){
        return book.remove(phoneNumber);
    }

    /**
     * Deletes all phone book entries of a specified person.
     * @param person of the entries which will be deleted
     * @return the phone numbers of the deleted entry
     */
    public Set<Integer> deleteEntriesOfAPerson(String person){
        
        Set<Integer> removedPhoneNumbers = new HashSet<Integer>();

        for(Map.Entry<Integer,String> entry : book.entrySet()) {
            if(entry.getValue().equals(person)){
                removedPhoneNumbers.add((Integer)entry.getKey());
            }
        }
        book.keySet().removeAll(removedPhoneNumbers);
        return removedPhoneNumbers;
    }

    /**
     * Deletes all entries of the phone book.
     */
    public void clearPhoneBook(){
        book.clear();
    }

    /**
     * Returns the map of the phone book.
     * @return map with contact entries
     */
    public SortedMap<Integer, String> getBook() {
        return book;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 17;
        result = prime * result + ((book == null) ? 0 : book.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PhoneBook other = (PhoneBook) obj;
        if (book == null) {
            if (other.book != null)
                return false;
        } else if (!book.equals(other.book))
            return false;
        return true;
    }

    
}
